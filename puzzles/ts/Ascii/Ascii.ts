const L: number = parseInt(readline());
const H: number = parseInt(readline());
const T: string = readline().toUpperCase();

for (let i = 0; i < H; i++) {
    const ROW: string = readline();
    let output: string = "";
    for (let letter of T) {

        let alphabetPosition = letter.charCodeAt(0) - 'A'.charCodeAt(0);
        if (alphabetPosition < 0 || alphabetPosition > 25) {
            alphabetPosition = 26;
        }
        let column = alphabetPosition * L;
        output += ROW.substring(column, column + L);

    }
    console.log(output);
}


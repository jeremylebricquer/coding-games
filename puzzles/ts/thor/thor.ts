/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/

var inputs: string[] = readline().split(' ');
const lightX: number = parseInt(inputs[0]); // the X position of the light of power
const lightY: number = parseInt(inputs[1]); // the Y position of the light of power
const initialTx: number = parseInt(inputs[2]); // Thor's starting X position
const initialTy: number = parseInt(inputs[3]); // Thor's starting Y position

let playerX = initialTx;
let playerY = initialTy;
// game loop
while (true) {

    let moveX = '';
    let moveY = '';
    const remainingTurns: number = parseInt(readline()); // The remaining amount of turns Thor can move. Do not remove this line.

    if (playerY > lightY) {
        moveY = 'N'
        playerY--;
    }
    else if (playerY < lightY) {
        moveY = 'S'
        playerY++;
    }
    if (playerX > lightX) {
        moveX = 'W';
        playerX ++;
    }
    else if (playerX < lightX){
        moveX= 'E';
        playerX--
    }


    console.error('playerY', playerY);
    console.error('playerX', playerX)

    console.error('lightY:',lightY);
     console.error('lightX:',lightX);
    // A single line providing the move to be made: N NE E SE S SW W or NW
    console.log(moveY+moveX);
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

const n: number = parseInt(readline()); // the number of temperatures to analyse
var inputs: string[] = readline().split(' ');
let temperatureClosestToZero: number = 5526;
for (let i = 0; i < n; i++) {
    const t: number = parseInt(inputs[i]);// a temperature expressed as an integer ranging from -273 to 5526
    const absoluteValue = Math.abs(t);
    const closestAbsoluteValue = Math.abs(temperatureClosestToZero);

    if (absoluteValue < closestAbsoluteValue) {
        temperatureClosestToZero = t
    } else if (absoluteValue == closestAbsoluteValue && t > temperatureClosestToZero) {
        temperatureClosestToZero = t;
    }
}
console.error('tab',inputs)
if (n === 0) {
    console.log(0);
} else {
    console.log(temperatureClosestToZero);
}
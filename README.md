# Solutions to Codingame Puzzles

This repository contains solutions to CodinGame puzzles.

## Puzzles

### Easy Puzzles
| Title | Solution(s) | Topic(s) |
| :---: | :------: | :------: |
| Variables, Input/Output, Conditions |
| The Descent | [TypeScript](./puzzles/ts/La descente/la-descente.ts) | Conditions, Loops |
| Power of Thor - Episode 1 | [TypeScript](./puzzles/ts/thor/thor.ts) | Conditions |
| Temperatures | [TypeScript](./puzzles/ts/temperatures/temperatures.ts)| Conditions, Loops , Array|
| ASCII | [TypeScript](./puzzles/ts/Ascii/Ascii.ts)| Loops|
| Mars-landers ep1| [TypeScript](./puzzles/ts/mars-lander/mars-landers.ts)| Conditions|
| Horse racing| [TypeScript](./puzzles/ts/horse-racing/horse-racing.ts)| Loops|
